package levelupday3;

public class ExceptionExample
{

    public static void main(String[] args)
    {

        ExceptionExample example = new ExceptionExample();
        try
        {
            example.doSomething();

        }
        catch (Exception err)
        {
            System.out.println("niemozna dizelic przez zero");

        }
    }

    public void doSomething() throws Exception
    {

        dzielPrzezZero();

    }

    public void dzielPrzezZero() throws Exception
    {
        long a = 0;
        try
        {
            a = 1 / 0;
        }
        catch (ArithmeticException err)
        {
            throw new Exception();

        }

        System.out.println(a);

    }
}
