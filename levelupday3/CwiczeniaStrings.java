package levelupday3;

public class CwiczeniaStrings
{

    public static void main(String[] args)
    {
        CwiczeniaStrings cwiczeniaStrings = new CwiczeniaStrings();

    }

    String linia1 = "Stoi na stacji lokomotywa, \n";
    String linie2 = "Ciężka, ogromna i POT z niej spływa: \n";
    String linia3 = "Tłusta oliwa. \n";
    String linia4 = "Stoi i sapie, dyszy i dmucha, \n";
    String linia5 = "Żar z rozgrzanego jej brzucha bucha: \n";
    
    /**
     * StringBuilder
     */
    public String laczyWszystkieLinieUzywajacStringBuildera() {
        return "";
    }

    /**
     * indexOf
     */
    public int znajdzPozycjePierwszegoPrzecinka() {
        return 0;
    }
    
    /**
     * split
     */
    public String[] rodzielaElementyZdaniaOddzielonePrzecinkiem() {
        return null;
    }


    public String zamieniaSlowoPotNaPisaneZMalejLitery() {
        return "";
    }


    /**
     * metoda ktory sprawdza czy string jest palindromem.
     * tzn. prawa i lewa strona stringa jest taka sama
     * 
     * przyklad kajak
     */
    String palindrom1 = "kajak";
    String palindrom2 = "kobyła ma mały bok";
    
    public boolean sprawdzaCzyStringJestPalindromem(String str){
        return false;
    }
    
}
