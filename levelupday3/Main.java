package levelupday3;

public class Main
{

    public static void main(String[] args)
    {
        int v = 5;
        int a = 0;
        
        innerMethod(v);
        returnValue();
        
        for (int i = 0;  i < 10; i++) {
            a = i;
        }
        
        System.out.println(v);
    }
    
    
    public static void innerMethod(int v) {
        System.out.println(v);
    }
    
    public static String returnValue() {
        return "this is a value returned by this method";
        
    }
    

}
