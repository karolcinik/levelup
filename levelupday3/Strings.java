package levelupday3;

public class Strings
{

    public static void main(String[] args)
    {
        System.out.println("sprawdza czy string zawiera (DUPA)."
                           .contains("xxx"));
    }
    
    

    public static void printWhitspaces() {
        System.out.println("");
        System.out.println("");
        System.out.print("tabulator    : \t tab 1 \t\t tab 2");
        
        System.out.println("");
        System.out.println("");
        System.out.print("break line   : line 1 \n line 2");
        
        System.out.println("");
        System.out.println("");
        System.out.print("caret return : line 1 \r line 2");
    }
    
    public static void convertFloatToString()
    {
        String pi = "3.142";
        float piF = Float.valueOf(pi);
        System.out.println(piF * 2);

    }

    // http://www.theasciicode.com.ar/ascii-printable-characters/capital-letter-a-uppercase-ascii-code-65.html
    public static void convertCharToByteAndPrint()
    {

        char c = 'A';
        byte b = (byte) c;

        System.out.println(Byte.valueOf(b));
    }
    
    
    public static void manipulators() {
        // zwraca pozycje znaku w stringu. Numeruje od 0
        System.out.println("123456789".indexOf("9"));
        
        // czesc stringa od 5 tego elementu
        System.out.println("123456789".substring(5));
        
        // czesc stringa pomiedzy elementami
        System.out.println("123456789".substring(2,7));
        
        // dzielenie stringa uzywajac elementu -
        String[] elementy = "1-2-3".split("-");
        System.out.println(elementy[0]);
        System.out.println(elementy[1]);
        System.out.println(elementy[2]);

        // laczy elementy przy pomocy stringa
        System.out.println(String.join("*", elementy));
        
        System.out.println("ten napis bedzie z wielkiej litery".toUpperCase());
        
        System.out.println("TEN NAPIS BEDZIE Z MALEJ litery".toUpperCase());
        
        
        System.out.println("zwraca pozycje znalezionego stringa w stringu".indexOf("pozy"));
        
        System.out.println("podmienia znaleziony string (DUPA).".replaceAll("DUPA", "****"));
        
        System.out.println("sprawdza czy string zawiera (DUPA).".contains("DUPA"));
        
    } 
    
    public static void stringBuilderExample() {
        StringBuilder stringBuilder = new StringBuilder();
        
        stringBuilder.append("line 1 \n");
        stringBuilder.append("line 2 \n");
        stringBuilder.append("line 3 \n");
        
        System.out.println(stringBuilder.toString());
        
    }
    

}
