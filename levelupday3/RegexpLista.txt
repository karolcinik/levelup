– \d – dowolna cyfra: [0-9]
– \D – dowolny znak nie będący cyfrą: [^0-9]
– \w – dowolna cyfra, litera (mała lub duża) lub znak podkreślenia: [0-9a-zA-Z_]
– \W – dowolny znak nie będący cyfrą, literą ani znakiem podkreślenia: [^0-9a-zA-Z_]
– \s – dowolny biały znak: [ \t\r\n\v\f]
– \S – dowolny znak nie-biały: [^ \t\r\n\v\f]

Znaki specjalne:

– \t – znak tabulacji poziomej (0x09)
– \v – znak tabulacji pionowej (0x0B)
– \r – „powrót karetki” (0x0D)
– \n – znak nowej linii (0x0A)
– \f – wysunięcie strony (0x0C)
Powtórzenia wzorca

W trakcie pisania wyrażeń regularnych często zdarza się, że pewien fragment wzorca powinien się powtórzyć. Służą do tego specjalne kwalifikatory, które umieszczone w wyrażeniu regularnym określają ile razy może (lub musi) powtórzyć się element znajdujący się tuż przed nim.

Oto lista:

– {n} – dokładnie n wystąpień poprzedzającego wyrażenia
– {n,} – n lub więcej wystąpień poprzedzającego wyrażenia
– {n,m} – od n do m wystąpień poprzedzającego wyrażenia
– ? – element poprzedzający jest opcjonalny (może wystąpić 0 lub 1 raz)
– + – element poprzedzający może wystąpić jeden lub więcej razy
– * – element poprzedzający może wystąpić zero lub więcej razy

- ^ poczatek lini
- $ koniec lini


przyklady:

znalezienie liczby.
text:    1234 ale liczby text
regexp1:  [0-9]
regexp2:  \d

^text - poczatek lini ^
text$

2016-05-02 jakis text
2016-05-02 jakis text nerka
2016-05-02 jakis text

AZN789456BC
[A-Z]+[0-9][A-Z]+

\d{4}-\d{2}-\d{2}.*NERKA

znalezienie ciagu liczb:
text:    1234 ale liczby
regexp1:  [0-9]{3}
regexp2:  \d{3}

znalezienie ciagu znakow:
text:    1234 Ale liczby abcdefghijklmn
regexp1:  [a-zA-Z]{12}

znalezienie daty
text:  2012-12-02 to jest data
regexp1:  [0-9]{4}-[0-9]{2}-[0-9]{2}
regexp2:  \d{4}-\d{2}-\d{2}

znalezienie wzorca w stringu
text: to jest przyklad dziwnego id: AF18-33aagg789B 
regexp1: [A-Z]{2}[0-9]{2}-[0-9]{2}[a-z]{4}[0-9]{3}[A-B]
regexp2: [A-Z]{2}\d{2}-\d{2}[a-z]{4}\d{3}[A-B]











